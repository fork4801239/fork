from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<h1>Esta é uma aplicação Web para testar O Docker na Pipeline!</h1>'


if __name__ == "__main__":
    app.run(debug=True)